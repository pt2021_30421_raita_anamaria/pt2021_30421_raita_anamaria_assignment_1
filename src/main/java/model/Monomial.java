package model;

import java.util.Objects;

public class Monomial implements Comparable<Monomial> {
    private double coefficient;
    private int exponent;

    public double getCoefficient() {
        return coefficient;
    }

    public void setCoefficient(double coefficient) {
        this.coefficient = coefficient;
    }

    public int getExponent() {
        return this.exponent;
    }

    public void setExponent(int exponent) {
        this.exponent = exponent;
    }

    public Monomial() {
    }

    public Monomial(double coefficient, int exponent) {
        this.coefficient = coefficient;
        this.exponent = exponent;
    }

    /**
     * This method stylizes the way in which a monomial will be displayed
     * to a user taking into account all possible cases.
     * @return a string that represents a readable form of a monomial
     */
    @Override
    public String toString() {
        if (this.coefficient - (long) this.coefficient == 0) {
            if (this.exponent == 0) {
                return (long) this.coefficient + "";
            }
            if (this.coefficient == 0) {
                return " ";
            }
            return (long) this.coefficient + "x^" + this.exponent;
        } else {
            if (this.exponent == 0) {
                return this.coefficient + "";
            }
            return this.coefficient + "x^" + this.exponent;
        }

    }

    /**
     *This method is used to sort the elements of a polynomial by their exponent in descending order.
     */
    @Override
    public int compareTo(Monomial o) {
        return o.getExponent() - this.exponent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Monomial monomial = (Monomial) o;
        return Double.compare(monomial.coefficient, coefficient) == 0 && exponent == monomial.exponent;
    }

    @Override
    public int hashCode() {
        return Objects.hash(coefficient, exponent);
    }
}
