package model;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Polynomial {
    private List<Monomial> polynomial;

    public Polynomial(Monomial... monomials) {
        this.polynomial = new ArrayList<>();
        Collections.addAll(this.polynomial, monomials);
        this.polynomial.sort(Monomial::compareTo);
    }

    /**
     * This constructor takes as an input a string which is supposed to be a polynomial.
     * It obtains all the terms from the polynomial if introduced correctly.
     * Otherwise it displays error messages.
     * In the end it builds the polynomial adding all the obtained monomials.
     */
    public Polynomial(String polynomial) {
        this.polynomial = new ArrayList<>();
        double coefficient = 0;
        int exponent = 0;
        String[] monomialsStrings = polynomial.split(" ");
        for (String monomialString : monomialsStrings) {
            String[] parameters = monomialString.split("X|x");
            try {
                coefficient = Double.parseDouble(parameters[0]);
                if (coefficient - (long) coefficient != 0) {
                    JOptionPane.showMessageDialog(null, "Error! You introduced real coefficients.They will be turned into integers");
                    coefficient = (long) coefficient;
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Error! Wrong format");
            }
            try {
                exponent = Integer.parseInt(parameters[1].substring(1));

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Error! Wrong format");
            }

            this.polynomial.add(new Monomial(coefficient, exponent));
        }
    }

    public Polynomial(List<Monomial> polynomial) {
        this.polynomial = new ArrayList<>(polynomial);
    }

    public List<Monomial> getPolynomial() {
        return polynomial;
    }

    public void setPolynomial(List<Monomial> polynomial) {
        this.polynomial = polynomial;
    }

    public void addMonomial(Monomial monomial) {
        if (this.polynomial == null) {
            this.polynomial = new ArrayList<>();
        }
        this.polynomial.add(monomial);
    }

    public void stylizedPolynomial() {
        this.getPolynomial().removeIf(monomial -> monomial.getCoefficient() == 0 && monomial.getExponent() != 0);
    }

    /**
     * This method does in general almost the same thing as its homologue from Monomial.
     * This method stylizes the way in which a polynomial will be displayed
     * to a user taking into account all possible cases.
     * @return a string which represents a readable form of a polynomial for the user
     */
    @Override
    public String toString() {
        final StringBuilder output = new StringBuilder();
        if (!this.getPolynomial().isEmpty()) {
            Monomial firsMonomial = this.polynomial.get(0);
            output.append(firsMonomial.toString());

            this.polynomial.remove(firsMonomial);
            for (Monomial monomial : this.getPolynomial()) {
                if (monomial.getCoefficient() < 0) {
                    output.append(" ").append(monomial.toString());
                }
                if (monomial.getCoefficient() > 0) {
                    output.append(" +").append(monomial.toString());
                }
                if (monomial.getCoefficient() == 0) {
                }
            }

            this.polynomial.add(firsMonomial);
            this.polynomial.sort(Monomial::compareTo);
        } else {
            output.append("0");
        }

        return output.toString();
    }
}
