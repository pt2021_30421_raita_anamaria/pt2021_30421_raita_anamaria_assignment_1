import model.Monomial;
import model.Polynomial;
import controller.Controller;

public class Main {

    public static void main(String[] args) {
        Monomial monomial = new Monomial(8, 4);
        System.out.println("Primul monom");
        System.out.println(monomial);
        Monomial derivedMonomial = Controller.deriveMonomial(monomial);
        System.out.println("Monom derivat");
        System.out.println(derivedMonomial);
        Monomial integratedMonomial = Controller.integrateMonomial(monomial);
        System.out.println("Monom integrat");
        System.out.println(integratedMonomial);

        Monomial monomial1 = new Monomial(-0.75, 7);
        System.out.println("Al doilea monom");
        System.out.println(monomial1);
        Monomial derivedMonomial1 = Controller.deriveMonomial(monomial1);
        System.out.println("Al doilea monom derivat");
        System.out.println(derivedMonomial1);
        Monomial integratedMonomial1 = Controller.integrateMonomial(monomial1);
        System.out.println("Al doilea monom integrat");
        System.out.println(integratedMonomial1);

        Polynomial polynomial = new Polynomial(monomial, derivedMonomial, integratedMonomial, derivedMonomial1, integratedMonomial1, monomial1);
        System.out.println("model.Polynomial 1: ");
        System.out.println(polynomial);

        Polynomial polynomial2 = new Polynomial(monomial, derivedMonomial, integratedMonomial, derivedMonomial1, integratedMonomial1);
        System.out.println("model.Polynomial 2: ");
        System.out.println(polynomial2);

        System.out.println("Added model.Polynomial:");
        //Polynomial addedPolynomial = Operations.multiplyPolynomial(polynomial, polynomial2);
        //  System.out.println(addedPolynomial);

        Polynomial subtractPolynomial = Controller.subtractPolynomial(polynomial, polynomial2);

        // System.out.println("model.Polynomial 1: ");
        //System.out.println(polynomial);
//aici cred ca se modifica ceva in polinom cand facem operatii si l am lasat doar ca sa vezi ca at cand am toti
//coeficientii 0 imi printeaza 0
        System.out.println("model.Polynomial 0: ");
        System.out.println(polynomial2);


        System.out.println("Subtracted model.Polynomial:");
        System.out.println(subtractPolynomial);
    }

}
