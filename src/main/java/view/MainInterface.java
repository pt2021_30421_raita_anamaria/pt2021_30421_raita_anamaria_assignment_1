package view;

import model.Polynomial;

import javax.swing.*;

import static controller.Controller.*;

public class MainInterface {
    private JPanel main;
    private JTextField textField1;
    private JTextField textField2;
    private JTextField result;
    private JButton quotentRemainderButton;
    private JButton productButton;
    private JButton deriveFirstButton;
    private JButton integrateFirstButton;
    private JButton sumButton;
    private JButton differenceButton;
    private JButton deriveSecondButton;
    private JButton integrateSecondButton;

    public static void main(String[] args) {
        JFrame frame = new JFrame("MainInterface");
        frame.setContentPane(new MainInterface().main);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        JOptionPane.showMessageDialog(null, "Real coefficients are automatically treated as integer ones!!!!!!");

    }

    public MainInterface() {
        textField1.setText("12x^5 +2x^3");
        textField2.setText("3x^3 -4x^1");
        sumButton.addActionListener(e -> result.setText(additionPolynomial(new Polynomial(textField1.getText()), new Polynomial(textField2.getText())).toString()));
        differenceButton.addActionListener(e -> result.setText(subtractPolynomial(new Polynomial(textField1.getText()), new Polynomial(textField2.getText())).toString()));
        deriveFirstButton.addActionListener(e -> result.setText(derivePolynomial(new Polynomial(textField1.getText())).toString()));
        deriveSecondButton.addActionListener(e -> result.setText(derivePolynomial(new Polynomial(textField2.getText())).toString()));
        integrateFirstButton.addActionListener(e -> result.setText(integratePolynomial(new Polynomial(textField1.getText())).toString()));
        integrateSecondButton.addActionListener(e -> result.setText(integratePolynomial(new Polynomial(textField2.getText())).toString()));
        productButton.addActionListener(e -> result.setText(multiply(new Polynomial(textField1.getText()), new Polynomial(textField2.getText())).toString()));
        quotentRemainderButton.addActionListener(e -> result.setText(division(new Polynomial(textField1.getText()), new Polynomial(textField2.getText()))));
    }
}
