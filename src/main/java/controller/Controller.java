package controller;

import model.Monomial;
import model.Polynomial;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Controller {
    public static Monomial deriveMonomial(Monomial monomial) {
        return new Monomial(monomial.getCoefficient() * monomial.getExponent(), monomial.getExponent() - 1);
    }

    public static Polynomial derivePolynomial(Polynomial polynomial) {
        Polynomial derivedPolynomial = new Polynomial();
        polynomial.getPolynomial().forEach(monomial -> derivedPolynomial.addMonomial(deriveMonomial(monomial)));
        return derivedPolynomial;
    }

    public static Monomial integrateMonomial(Monomial monomial) {
        return new Monomial(monomial.getCoefficient() / (monomial.getExponent() + 1), monomial.getExponent() + 1);
    }

    public static Polynomial integratePolynomial(Polynomial polynomial) {
        Polynomial integratedPolynomial = new Polynomial();
        polynomial.getPolynomial().forEach(monomial -> integratedPolynomial.addMonomial(integrateMonomial(monomial)));
        return integratedPolynomial;
    }

    /**
     *This method takes two polynomials as parameters 2 polynomials and returns another
     *polynomial which returns the sum between those 2.
     * @param firstPolynomial
     * @param secondPolynomial
     * @return a polynomial
     */
    public static Polynomial additionPolynomial(Polynomial firstPolynomial, Polynomial secondPolynomial) {
        List<Monomial> sum = new ArrayList<>(firstPolynomial.getPolynomial());
        List<Integer> addedPolynomialExponents = sum.stream().map(Monomial::getExponent).collect(Collectors.toList());

        secondPolynomial.getPolynomial().forEach(secondMonomial -> {
            if (addedPolynomialExponents.contains(secondMonomial.getExponent())) {
                sum
                        .stream()
                        .filter(monomial -> monomial.getExponent() == secondMonomial.getExponent())
                        .findFirst()
                        .ifPresent(monomial -> monomial.setCoefficient(monomial.getCoefficient() + secondMonomial.getCoefficient()));
            } else {
                sum.add(secondMonomial);
            }
        });
        sum.sort(Monomial::compareTo);
        return new Polynomial(sum);
    }

    /**
     *This method takes two polynomials as parameters 2 polynomials and returns another
     *polynomial which returns the difference between those 2.
     * @param minuend
     * @param subtrahend
     * @return a polynomial
     */
    public static Polynomial subtractPolynomial(Polynomial minuend, Polynomial subtrahend) {
        List<Monomial> difference = new ArrayList<>(minuend.getPolynomial());
        List<Integer> subtractedPolynomialExponents = difference.stream().map(Monomial::getExponent).collect(Collectors.toList());

        subtrahend.getPolynomial().forEach(subtrahendMonomial -> {
            if (subtractedPolynomialExponents.contains(subtrahendMonomial.getExponent())) {
                difference.stream()
                        .filter(monomial -> monomial.getExponent() == subtrahendMonomial.getExponent())
                        .findFirst()
                        .ifPresent(monomial -> monomial.setCoefficient(monomial.getCoefficient() - subtrahendMonomial.getCoefficient()));
            } else {
                subtrahendMonomial.setCoefficient(subtrahendMonomial.getCoefficient() * -1);
                difference.add(subtrahendMonomial);
            }
        });
        difference.sort(Monomial::compareTo);
        Polynomial result = new Polynomial(difference);
        result.stylizedPolynomial();
        return result;
    }

    /**
     *This method takes two polynomials as parameters 2 polynomials and returns another
     *polynomial which returns the product between those 2.
     * @param firstPolynomial
     * @param secondPolynomial
     * @return a polynomial
     */
    public static Polynomial multiply(Polynomial firstPolynomial, Polynomial secondPolynomial) {
        List<Monomial> result = new ArrayList<>();
        firstPolynomial.getPolynomial().forEach(firstMonomial -> secondPolynomial.getPolynomial().forEach(secondMonomial -> {
            Monomial resultMonomial = new Monomial(firstMonomial.getCoefficient() * secondMonomial.getCoefficient(), firstMonomial.getExponent() + secondMonomial.getExponent());
            List<Integer> resultExponents = result.stream().map(Monomial::getExponent).collect(Collectors.toList());
            if (resultExponents.contains(resultMonomial.getExponent())) {
                result.stream()
                        .filter(monomial -> monomial.getExponent() == resultMonomial.getExponent())
                        .findFirst()
                        .ifPresent(monomial -> monomial.setCoefficient(monomial.getCoefficient() + resultMonomial.getCoefficient()));
            } else {
                result.add(resultMonomial);
            }
        }));
        result.sort(Monomial::compareTo);
        return new Polynomial(result);
    }

    /**
     *This method takes two polynomials as parameters 2 polynomials and returns another
     *polynomial in form of a string which returns the quotient and a remainder between those two
     * if it is possible to divide the firstPolynomial by the secondPolynomial.
     * @param firstPolynomial
     * @param secondPolynomial
     * @return a string which represents the quotient and the remainder
     */
    public static String division(Polynomial firstPolynomial, Polynomial secondPolynomial) {
        Polynomial quotient = new Polynomial();
        Polynomial remainder = new Polynomial();

        remainder.setPolynomial(firstPolynomial.getPolynomial());
        if (remainder.getPolynomial().get(0).getExponent() < secondPolynomial.getPolynomial().get(0).getExponent()) {
            JOptionPane.showMessageDialog(null, "Error!Division impossible");
            return "";
        }

        while (remainder.getPolynomial().get(0).getExponent() >= secondPolynomial.getPolynomial().get(0).getExponent()) {
            Monomial aux = new Monomial(remainder.getPolynomial().get(0).getCoefficient() / secondPolynomial.getPolynomial().get(0).getCoefficient(),
                    remainder.getPolynomial().get(0).getExponent() - secondPolynomial.getPolynomial().get(0).getExponent());
            quotient.addMonomial(aux);
            Polynomial copyQuotient = new Polynomial();
            copyQuotient.addMonomial(aux);
            Polynomial auxiliary2 = multiply(secondPolynomial, copyQuotient);
            remainder = subtractPolynomial(remainder, auxiliary2);
        }

        StringBuilder aux2 = new StringBuilder();
        aux2.append("Quotient is:   ").append(quotient.toString()).append("  Remainder is:  ").append(remainder.toString());
        return aux2.toString();
    }
}
