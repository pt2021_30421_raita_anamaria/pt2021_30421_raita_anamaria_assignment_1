package controller;
import model.Polynomial;
import org.junit.jupiter.api.Test;

import static controller.Controller.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
class TestController {
    @Test
    void testAddition(){
        Polynomial firstPolynomial=new Polynomial("12x^5 +2x^3");
        Polynomial secondPolynomial=new Polynomial("3x^3 -4x^1");
        Polynomial result=additionPolynomial(firstPolynomial,secondPolynomial);
        assertEquals("12x^5 +5x^3 -4x^1",result.toString());
    }
    @Test
    void testSubtraction(){
        Polynomial firstPolynomial=new Polynomial("12x^5 +2x^3");
        Polynomial secondPolynomial=new Polynomial("3x^3 -4x^1");
        Polynomial result=subtractPolynomial(firstPolynomial,secondPolynomial);
        assertEquals("12x^5 -1x^3 +4x^1",result.toString());
    }
    @Test
    void testMultiplication(){
        Polynomial firstPolynomial=new Polynomial("12x^5 +2x^3");
        Polynomial secondPolynomial=new Polynomial("3x^3 -4x^1");
        Polynomial result=multiply(firstPolynomial,secondPolynomial);
        assertEquals("36x^8 -42x^6 -8x^4",result.toString());
    }
    @Test
    void testDivision(){
        Polynomial firstPolynomial=new Polynomial("12x^5 +2x^3");
        Polynomial secondPolynomial=new Polynomial("3x^3 -4x^1");
        String result= division(firstPolynomial,secondPolynomial);
        assertEquals("Quotient is:   4x^2 +6  Remainder is:  24x^1",result);
    }
    @Test
    void testDerivation(){
        Polynomial firstPolynomial=new Polynomial("12x^5 +2x^3");
        Polynomial result= derivePolynomial(firstPolynomial);
        assertEquals("60x^4 +6x^2",result.toString());
    }
    @Test
    void testDerivation2(){
        Polynomial firstPolynomial=new Polynomial("3x^3 -4x^1");
        Polynomial result= derivePolynomial(firstPolynomial);
        assertEquals("9x^2 -4",result.toString());
    }
    @Test
    void testIntegration(){
        Polynomial firstPolynomial=new Polynomial("12x^5 +2x^3");
        Polynomial result= integratePolynomial(firstPolynomial);
        assertEquals("2x^6 +0.5x^4",result.toString());
    }
    @Test
    void testIntegration2(){
        Polynomial firstPolynomial=new Polynomial("3x^3 -4x^1");
        Polynomial result= integratePolynomial(firstPolynomial);
        assertEquals("0.75x^4 -2x^2",result.toString());
    }
}
